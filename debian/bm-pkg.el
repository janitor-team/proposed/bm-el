(define-package "bm"
  "201905" "Visible bookmarks in buffer."
  'nil
  :keywords '("bookmark" "highlight" "faces" "persistent")
  :url "https://github.com/joodland/bm")
